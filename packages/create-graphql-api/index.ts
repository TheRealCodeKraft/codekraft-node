#!/usr/bin/env node

import chalk from 'chalk'
import Commander from 'commander'
import path from 'path'

import packageJson from './package.json'
import createApi from './create-api'

let projectPath: string = ''

const program = new Commander.Command(packageJson.name)
	.version(packageJson.version)
	.arguments('<project-directory>')
	.usage(`${chalk.green('<project-directory>')}`)
  .action(name => {
    projectPath = name
  })
	.parse(process.argv)

async function run() {
  if (!projectPath) {
    console.log()
    console.log('Please specify the project directory:')
    console.log(
      `  ${chalk.cyan(program.name())} ${chalk.green('<project-directory>')}`
    )
    console.log()
    console.log('For example:')
    console.log(`  ${chalk.cyan(program.name())} ${chalk.green('api')}`)
    console.log()
    console.log(
      `Run ${chalk.cyan(`${program.name()} --help`)} to see all options.`
    )
    process.exit(1)
  }

  const resolvedProjectPath = path.resolve(projectPath)
  const projectName = path.basename(resolvedProjectPath)

  await createApi({
    apiPath: resolvedProjectPath,
  })
}

run()
	.catch(async reason => {
		console.log()
		console.log('Aborting installation.')
		if (reason.command) {
			console.log(`  ${chalk.cyan(reason.command)} has failed.`)
		} else {
			console.log(chalk.red('Unexpected error. Please report it as a bug:'))
			console.log(reason)
		}
		console.log()

		process.exit(1)
	})
