// Transpile all code following this line with babel and use '@babel/preset-env' (aka ES6) preset.
require("@babel/register")({
	presets: ["@babel/preset-env"],
	plugins: [
		"@babel/plugin-proposal-class-properties",
		[
			"@babel/plugin-transform-runtime",
			{
				"regenerator": true
			}
		]
	]
})

//Allow to absolute path import in the project
const path = require('path')
const basePath = path.join(__dirname, '..')
require('app-module-path').addPath(basePath)

// Import the rest of our application.
module.exports = require('./bootstrap.js')
