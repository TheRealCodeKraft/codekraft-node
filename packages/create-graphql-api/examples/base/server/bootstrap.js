
import express from 'express'
import express_graphql from 'express-graphql'
import { graphqlUploadExpress } from 'graphql-upload'
import { buildSchema } from 'graphql'
const cors = require('cors');
import expressPlayground from 'graphql-playground-middleware-express'

import FormatError from 'easygraphql-format-error'
const formatError = new FormatError()
const errorName = formatError.errorName

var app = express();
var bodyParser = require('body-parser')

app.use(cors())
app.use(bodyParser.json({limit: '50mb'}))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))

import { getUser, schema } from '../src/graphql'
app.use(
	'/graphql',
	graphqlUploadExpress({ maxFileSize: 10000000, maxFiles: 10 }),
	express_graphql((req, res, params) => ({
		schema: schema,
		graphiql: false,
		context: {
			errorName,
			req,
			res,
			headers: req.headers,
			user: getUser(req, res, params)
		},
		customFormatErrorFn: (err) => {
			return formatError.getError(err)
		}
	})
))

const playground = expressPlayground({
	endpoint: '/graphql',
	settings: {
		'editor.theme': 'light',
		'schema.polling.interval': 5000,
		'editor.cursorShape': 'block'
	}
})
app.get('/playground', playground)

const packageJson = require('package.json')
const PORT = process.env.PORT || 3000
app.listen(PORT, () => console.log(`${packageJson.name} ${packageJson.version} Server Now Running On localhost:${PORT}/graphql`))
