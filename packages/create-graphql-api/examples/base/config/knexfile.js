
const path = require('path');
const BASE_PATH = path.join(__dirname, '../src/db')

const dotenv = require('dotenv')
dotenv.config({ path: path.join(process.cwd(), '..', '.env') })

module.exports = {

  development: {
    client: 'pg',
    connection: process.env.DATABASE_URL,
		migrations: {
			directory: path.join(BASE_PATH, 'migrations')
		},
		seeds: {
			directory: path.join(BASE_PATH, 'seeds')
		}
  },

  staging: {
		client: 'pg',
		connection: process.env.DATABASE_URL,
		migrations: {
			directory: path.join(BASE_PATH, 'migrations')
		},
		seeds: {
			directory: path.join(BASE_PATH, 'seeds')
		}
  },

  production: {
    client: 'pg',
		connection: process.env.DATABASE_URL,
		migrations: {
			directory: path.join(BASE_PATH, 'migrations')
		},
		seeds: {
			directory: path.join(BASE_PATH, 'seeds')
		}
  }

}
