exports.up = (knex) => {
  return knex.schema.raw(`
		CREATE TABLE users (
			id uuid primary key,
			firstname text not null,
			lastname text not null,
			email text not null unique,
			password_digest text not null
		)
	`)
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTable('users')
}
