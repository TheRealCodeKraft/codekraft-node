
const knex = require('./connection')

export const getUsers = (filters) => {
  return knex('users')
		.select('*')
		.where(filters)
}

export const getUser = (id) => {
	return knex('users')
		.where({id})
		.first()
}
