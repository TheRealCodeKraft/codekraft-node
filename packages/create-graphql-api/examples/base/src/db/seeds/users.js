
const user = require('../../models/user')
const uuidv1 = require('uuid/v1')

const packageJson = require('../../../package.json')

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
			return knex('users').insert([
				{
					id: uuidv1(),
					firstname: 'God',
					lastname: 'Admin',
					email: `god@${packageJson.name.replace(/-.*$/, '')}.com`,
					password_digest: user.hashPassword('god')
				}
			])
    })
}
