
const knex = require('./connection')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

export const login = async (args) => {
	const user = await knex('users')
		.select('*')
		.where({email: args.email})
		.first()
  if (!user) {
    throw new Error('No such user found')
  }

  const valid = await bcrypt.compare(args.password, user.password_digest)
  if (!valid) {
    throw new Error('Invalid password')
  }

  const token = jwt.sign({ userId: user.id }, process.env.APP_SECRET)

  return {
    token,
    user,
  }
}
