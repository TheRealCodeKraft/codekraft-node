
const jwt = require('jsonwebtoken')

const checkToken = (context) => {
	if (!context || !context.headers || !context.headers.authorization) {
		return false
	}

	const token = context.headers.authorization || ''

	try {
		return jwt.verify(token.split(' ')[1], process.env.APP_SECRET)
	} catch (e) {
		return false
	}
}

const getUser = (req, res, params) => {
	if (!req || !req.headers || !req.headers.authorization) {
		return null
	}

	const token = req.headers.authorization || ''

	try {
		return jwt.verify(token.split(' ')[1], process.env.APP_SECRET)
	} catch (e) {
		return null
	}
}


module.exports = {
  getUser,
	checkToken
}
