
import { getUser, checkToken } from './auth'

const schema = require('./middlewares')

module.exports = {
	schema,
	checkToken,
	getUser
}
