
require('dotenv').config()

const { readFileSync } = require('fs')
const { makeExecutableSchema } = require('graphql-tools')

import { checkToken } from './auth'

const queries = require('../db/queries')
const mutations = require('../db/mutations')

const schema = makeExecutableSchema({
  typeDefs: readFileSync('src/graphql/schemas.graphql', 'utf8'),
  resolvers: {
    Query: {
      users: (_, filters) => queries.getUsers(filters),
			me: (_, filters, ctx) => queries.getUser(ctx.user.userId),
		},
		Mutation: {
			login: (_, args, context, info) => mutations.login(args),
		}
	},
	directiveResolvers: {
		authenticated: (next, source, args, context) => {
			if (checkToken(context)) {
				return next()
			}
			else {
				throw new Error(context.errorName.UNAUTHORIZED)
			}
		}
	}
})

module.exports = schema
