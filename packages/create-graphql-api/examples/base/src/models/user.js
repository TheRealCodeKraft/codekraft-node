
const bcrypt = require("bcrypt")

const hashPassword = (password, length = 10) => {
	return bcrypt.hashSync(password, length)
}

module.exports = {
	hashPassword
}
