
import chalk from 'chalk'
import spawn from 'cross-spawn'

export default function install(
  root: string
) {
  return new Promise((resolve, reject) => {
    let command: string
    let args: string[]

    command = 'yarnpkg'
    args = ['install']
    args.push('--cwd', root)

    const child = spawn(command, args, {
      stdio: 'inherit',
      env: { ...process.env, ADBLOCK: '1', DISABLE_OPENCOLLECTIVE: '1' },
    })
    child.on('close', (code: number) => {
      if (code !== 0) {
        reject({ command: `${command} ${args.join(' ')}` })
        return
      }
      resolve()
    })
  })
}
