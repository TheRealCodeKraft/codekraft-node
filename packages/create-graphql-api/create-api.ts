
import chalk from 'chalk'
import fs from 'fs'
import os from 'os'
import path from 'path'
import { ncp as originalNcp } from 'ncp'
import util from 'util'

const ncp = util.promisify(originalNcp)

import install from './install'

export default async function createApi({
	apiPath
}: {
	apiPath: string
}) {
	const root = path.resolve(apiPath)
	const apiName = `${path.basename(path.dirname(root))}-${path.basename(root)}`

	console.log()
	console.log(`Creating a new codekraft api in ${chalk.green(root)}.`)
	console.log()

	process.chdir(root)

	await ncp(path.join(__dirname, '..', 'examples', 'base'), root)

	const packageJson = require('./examples/base/package.json')
	packageJson.name = apiName

	fs.writeFileSync(
		path.join(root, 'package.json'),
		JSON.stringify(packageJson, null, "\t") + os.EOL
	)

	console.log()
	console.log('Installing packages. This might take a couple of minutes.')
	console.log()

	await install(root)
}
