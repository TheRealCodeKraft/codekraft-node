
import fs from 'fs'
import path from 'path'
import express from 'express'
import { ApolloServer } from 'apollo-server-express'

export default async function server(schema) {
	const server = new ApolloServer(schema)

	const app = express()
	server.applyMiddleware({ app })

	const port = process.env.PORT || 3000
	app.listen({ port }, () => {
  	console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`)
	})
}
